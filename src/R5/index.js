import * as pages from '../pages/index'
import {
  observable,
  autorun,
  computed,
  asFlat,
  asReference,
  asMap
} from 'mobx'

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
    return -1;
}

class App {
  constructor(){}
  @observable isLoading = 'not_loading';
  @observable doctree = asMap({})
  @observable helpers = asMap({})

  @observable registerPOM(doc){
    // console.log(doc, 'added to stack');
    this.doctree.set(doc.identifier, doc)
  }

  @observable registerHelper(helper){
    // console.log(helper, 'the helper added to stack');
    this.helpers.set(helper.identifier, helper)
  }

  alert(msg){
    alert(msg)
  }
}

let app = new App()

export default app;
