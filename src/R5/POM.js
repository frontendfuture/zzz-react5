import { action, asMap, computed, extendObservable } from 'mobx';
import React from 'react';

import R5 from './';
import app from './index'

let defaultProps = {
  value: ''
}

export default class POM {
  constructor(definition){
    let state = {}
    extendObservable(state, {
      props: null,
      childrenTree: null,
      childrenTreeAsMap: asMap({}),
      subscribers: asMap({}),
      config: null
    })
    this.props = state.props;
    this.childrenTree = state.childrenTree;
    this.childrenTreeAsMap = state.childrenTreeAsMap
    this.subscribers = state.subscribers
    this.config = state.config

    this.identifier = definition.identifier
    this.definition = definition;
    this.app = R5;
    this.props = Object.assign({}, defaultProps, definition.props);
    this.config = definition.config;
    this.component = require('../components/' + definition.type + '/index').default
  }

  @computed get renderComponent(){
    return (<div>
      <this.component
        {...this.props} {...this.eventsMap} app={R5} pom={this} key={this.identifier}
      >
      {this.parseChildren()}
    </this.component>
      </div>)
  }

  @action setProp(prop, value){
    this.props[prop] = value
  }

  @action getProp(prop){
    return this.props[prop]
  }

  @action setChildren(children){
    if(children && children.length){
      /* set childrenTree */
      this.childrenTree = children.map((childDef, index)=>{
        var childNode = new POM(childDef)
            childNode.setChildren(childDef.children)
            app.registerPOM(childNode)
        return childNode
      })
      /* set childrenTreeAsMap */
      this.childrenTree.forEach((child, index)=>{
        this.childrenTreeAsMap.set(child.identifier, child)
      })
    } else {
      this.childrenTree = []
    }
  }

  parseChildren(){
    return this.childrenTree.map((child, index)=>{
      return (<div key={child.identifier}>
        {child.renderComponent}
      </div>)
    })
  }


  objectify(string_identifier){
    let actor = string_identifier
    if(string_identifier.indexOf('pom_') == 0){
      actor = this.app.doctree.get(string_identifier.replace('pom_', ''))
    }
    if(string_identifier.indexOf('app_') == 0){
      actor = this.app;
    }

    if(string_identifier.indexOf('helper_') == 0 ){
      actor = this.app.helpers[string_identifier.replace('helper_', '')]
    }
    return actor
  }

  paramify(param){
    // console.log(param, 'PARAM');
    if(param.indexOf('::') > -1){
      let params = param.split('::')
      let actor = this.objectify(params[0])
      let prop = params[1]
      var result =  actor.getProp(prop)
      // console.log(result, 'paramify', prop, actor);
      return result;
    } else {
      return param
    }
  }

  @computed get eventsMap(){
    let evDef = this.definition.on;
    let functionifiedObj = {}
    for(var ev in evDef){
      functionifiedObj['on' + ev.charAt(0).toUpperCase() + ev.substr(1).toLowerCase()] = this._functionify(evDef[ev])
    }
    return functionifiedObj
  }

  _functionify(def){
    var objectified = {}

    function handler(e){
      e.stopPropagation()
      let actor = this
      for(var obj in def){
        actor = this.objectify(obj)
        let methodDef = def[obj]
        methodDef.forEach((action, index)=>{

          for(var method in action){
            actor[method](...action[method].map((param)=>{return this.paramify(param)}))
          }
        })
      }
    }
    return handler.bind(this)
  }


  /**
   * DO we use this?
   */
  @action registerAllDocs(){
    var results = []

    function traverse(parent){
      parent.childrenTree.map((child, index)=>{
        results.push(child)
        if(child.children && child.children.length){
          traverse(child)
        }
      })
    }

    traverse(this)
    let resultsObj = {}
    results.forEach((comp, index)=>{
      resultsObj[comp.identifier] = comp;
      this.app.doctree.set(comp.identifier, comp)
    })

    return resultsObj;
  }

  on(eventName, callback){
    if(!this.subscribers.get(eventName)){
      this.subscribers.set(eventName, [])
    }
    this.subscribers.get(eventName).push(callback)
  }

  publish(eventName, data=null){
    if(this.subscribers.get(eventName)){
      this.subscribers.get(eventName).forEach((callback)=>{
        callback()
      })
    }
  }

}
