import $ from 'jquery'

class R5Ajax {
  constructor(){
    this.identifier = 'R5Ajax'
  }

  post(url, data = null){
    console.log('posting ajax....', data);
    return $.ajax({
      method: 'POST',
      url: url,
      data: data
    })
  }

  put(url, data = null){
    console.log('putting ajax')
    return $.ajax({
      method: 'PUT',
      url: url,
      data: data
    })
  }
}

export default new R5Ajax()
