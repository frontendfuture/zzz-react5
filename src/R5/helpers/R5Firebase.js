import * as firebase from 'firebase'
import {computed, observable, action, asReference, asMap} from 'mobx'

export default class R5Firebase {
  constructor(config){
    this.identifier = 'R5Firebase'
    firebase.initializeApp(config)
    this.firebase = firebase
    return this
  }

  getDatabase(){
    return this.firebase.database()
  }
}
