export default {
  identifier: 'magic',
  type: 'PageSidebar',
  config: {
    menus: [
      {
        label: 'Dashboard',
        open: 'http://something.com/something',
        iconClass: 'fa fa-fw fa-home'
      },
      {
        label: 'Book',
        open: 'magictable',
        iconClass: 'fa fa-fw fa-book'
      },
      {
        label: 'Movie',
        open: 'magictable2',
        iconClass: 'fa fa-fw fa-play-circle'
      },
      {
        label: 'MENU 3',
        open: 'http://google.com',
        iconClass: 'fa fa-fw fa-home'
      }
    ],
  },
  props: {
    title: 'AMAZING APP',
  },
  children: [{
    identifier: 'magictable',
    type: 'R5Magic',
    config: {
      endpoint: 'http://localhost:1337/book',
    },
    props: {
      title: 'THE PAGE'
    }
  },
  {
    identifier: 'magictable2',
    type: 'R5Magic',
    config: {
      endpoint: 'http://localhost:1337/movie',
    },
  }]
}
