export default {
  identifier: 'todos',
  type: 'Page',
  children: [
    {
      identifier: 'row',
      type: 'Row',
      children: [
        {
          type: 'Col',
          identifier: 'col',
          props: {
            className: 'col-md-8 col-md-push-2'
          },
          children: [
            {
              identifier: 'heading',
              type: 'Heading',
              config: {
                variant: 'h1',
              },
              props: {
                title: 'The Form'
              }
            },
            {
              identifier: 'magictable',
              type: 'InputNumber',
              on: {
                change: {
                  pom_todos: [{setProp: ['title', 'pom_magictable::value']}]
                }
              }
            },
            {
              identifier: 'input2',
              type: 'Selection',
              props: {
                placeholder: 'Pilih salah satu di sini'
              },
              config: {
                defaultOptions: [{label: 'LABEL', value: 1}, {label: 'Label 2', value: 2}]
              }
            },
            {
              identifier: 'textarea1',
              type: 'TextArea',
              props: {
                placeholder: 'Ketikan sesuatu'
              }
            },
            {
              identifier: 'submitbtn',
              type: 'Button',
              props: {
                className: 'btn btn-primary btn-block',
                label: 'Kirim'
              }
            }
          ]
        }
      ]
    }
  ]
}
