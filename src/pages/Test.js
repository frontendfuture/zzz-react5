export default {
  "identifier": "book",
  "type": "SailsCRUD",
  "config": {
    "endpoint": "http://localhost:1337/book"
  },
  "children": [
    {
      "identifier": "crud_book",
      "type": "Page",
      "props": {
        "title": "CRUD book"
      },
      "children": [
        {
          "identifier": "FormCRUD_book",
          "type": "FormAjax",
          "children": [
            {
              "identifier": "title",
              "type": "Input",
              "props": {
                "placeholder": "Title"
              }
            },
            {
              "identifier": "author",
              "type": "Selection",
              "props": {
                "placeholder": "Author"
              },
              "config": {
                "defaultOptions": [
                  {
                    "label": "LABEL",
                    "value": 1
                  },
                  {
                    "label": "Label 2",
                    "value": 2
                  }
                ]
              },
            },
            {
              "identifier": "year",
              "type": "Input",
              "props": {
                "placeholder": "Year"
              }
            }
          ]
        }
      ]
    }
  ]
}
