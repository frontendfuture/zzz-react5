import Employee from './Employee'
import Todos from './Todo'
import TodoList from './TodoList'
import Test from './Test'

let pages = {}
pages[Employee.identifier] = Employee
pages[Todos.identifier] = Todos
pages[TodoList.identifier] = TodoList
pages[Test.identifier] = Test;

export {pages}
