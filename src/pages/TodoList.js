
/**
 * R5 Framework by Muhammad Azamuddin
 * Work in Progress.
 * mas.azamuddin@gmail.com
 */

export default {
  identifier: "todolist",
  type: "Page",
  children: [
    {
      identifier: 'easytodo',
      type: "TodoList",
      ref: "todos",
      inputPlaceholder: "Think before make task!",
      props: {
        "title": "Awesome Tasks"
      }
    }
  ]
}
