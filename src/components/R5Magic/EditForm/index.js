import { observable } from 'mobx';
import { observer } from 'mobx-react'
import React from 'react';

import BaseComponent from '../../BaseComponent/index'
import POM from '../../../R5/POM';

@observer
export default class EditForm extends BaseComponent {
  constructor(props) {
    super(props);
  }

  @observable Page

  componentWillReceiveProps(nextProps) {
    this.loadFields()
  }

  render() {
    return (<div style={this.props.style}>
      {this.Page && this.Page.childrenTree ? this.Page.renderComponent : ''}
    </div>);
  }

  loadFields(){
    $.ajax({
      method: 'GET',
      url: this.POM.config.endpoint + '/read-structure',
      success: (response)=>{
        let definition = Object.assign({}, response)
        if(!definition.children[0].config){definition.children[0].config = {}}
        definition.children[0].config.endpoint = this.POM.config.endpoint;
        definition.children[0].config.model_id = this.props.item ? this.props.item.id : ''
        this.Page = new POM(definition)
        definition.children[0].children.push({
          identifier: 'submit-btn',
          type: 'Button',
          props: {
            label: 'Simpan'
          }
        })
        definition.children[0].children = definition.children[0].children.map((child, index)=>{
          if(this.props.item){
            if(this.props.item[child.identifier]){
              child.props.value = this.props.item[child.identifier]
            }
          }
          return child
        })
        !definition.children[0].props ? definition.children[0].props = {method: 'PUT'} : definition.children[0].props.method = 'PUT'
        if(definition.children[0]){
          let clickHandler = {}
              clickHandler['pom_' + definition.children[0].identifier] = [{requestReload: []}]
          definition.children[0].on = {
            submit: clickHandler
          }
        }
        this.Page.setChildren(definition.children)
        this.Page.childrenTreeAsMap.get(definition.children[0].identifier).on("submit::success", ()=>{
          this.props.onChangeScreen('table')
          this.props.onReload()
        })
        this.props.app.registerPOM(this.Page)
      }
    })
  }

  requestReload(){
    console.log('REQUEST RELOAD');
  }
}

EditForm.propTypes = {
};
