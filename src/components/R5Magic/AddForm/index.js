import { observable } from 'mobx';
import { observer } from 'mobx-react'
import React from 'react';

import BaseComponent from '../../BaseComponent/index'
import POM from '../../../R5/POM';

@observer
export default class AddForm extends BaseComponent {
  constructor(props) {
    super(props);
  }

  @observable Page;

  render() {
    return (<div style={this.props.style}>
      {this.Page && this.Page.childrenTree ? this.Page.renderComponent : ''}
    </div>);
  }

  componentWillReceiveProps() {
    this.loadFields()
  }

  loadFields(){
    $.ajax({
      method: 'GET',
      url: this.POM.config.endpoint + '/read-structure',
      success: (response)=>{
        let definition = Object.assign({}, response)
        if(!definition.children[0].config){definition.children[0].config = {}}
        definition.children[0].config.endpoint = this.POM.config.endpoint
        this.Page = new POM(definition)
        definition.children[0].children.push({
          identifier: 'submit-btn',
          type: 'Button',
          props: {
            label: 'Simpan'
          }
        })
        this.Page.setChildren(definition.children)
        this.Page.childrenTreeAsMap.get(definition.children[0].identifier).on('submit::success', ()=>{
          this.props.onChangeScreen('table');
          this.props.onReload()
        })
        this.props.app.registerPOM(this.Page)
      }
    })
  }
}

AddForm.propTypes = {
};
