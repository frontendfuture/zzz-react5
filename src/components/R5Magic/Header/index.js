import React, {PropTypes} from 'react';
import BaseComponent from '../../BaseComponent/index'
import { observer } from 'mobx-react'

@observer
export default class Header extends BaseComponent {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    return (<div style={this.props.style}>
      <button className="btn btn-primary btn-xs" onClick={()=>{this.props.onChangeScreen('table')}}>List</button>
      <button className="btn btn-primary btn-xs" onClick={()=>{this.props.onChangeScreen('adding')}}>Add</button>
      <br/>
    </div>);
  }
}

Header.propTypes = {
};
