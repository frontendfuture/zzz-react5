import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'

export default class Col extends BaseComponent {
  constructor(props) {
    super(props);
  }


  render() {
    return (<div {...this.requestProps()}>
      {this.props.children}
    </div>);
  }
}

Col.propTypes = {
};
