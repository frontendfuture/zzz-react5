import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'
import {observable} from 'mobx'
import moment from 'moment-timezone'
moment.locale('id')

let defaultProps = {
  method: 'POST'
}
export default class Form extends BaseComponent {
  constructor(props) {
    super(props);
    this.config.endpoint = props.pom.definition.endpoint
  }

  /**
   * Observable to save optimistic update data;
   * when update fails, recoverDataMap use this observable to recover data
   */
  @observable previousDataMap
  @observable config = {}


  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div>
      <form {...this.extendedRequestProps()} onSubmit={this.mergeHandler.bind(this, 'onSubmit')}>
        {this.props.children}
      </form>
    </div>);
  }

  getDataMap(){
    var data = {}
    var children = this.POM.childrenTreeAsMap.toJS();
    for(var child in children){
      if(children[child].getProp('value')){
        data[child] = children[child].getProp('value')
        /** add timestamp **/
        data['timestamp'] = moment().unix()
      }
    }
    /** merge defaultValue from definition*/
    if(this.POM.config.defaultValue){
      data = Object.assign({}, this.POM.config.defaultValue, data)
    }


    return data;
  }

  clearDataMap(){
    this.previousDataMap = this.getDataMap()
    let children = this.POM.childrenTreeAsMap.toJS()
    for(var child in children){
      if(children[child].getProp('value')){
        children[child].setProp('value', '')
      }
    }
  }

  recoverDataMap(){
    for(var child in children){
      if(children[child].getProp('value')){
        children[child].setProp('value', this.previousDataMap[child])
      }
    }
  }

  onSubmit(e){

  }
}

Form.propTypes = {
};
