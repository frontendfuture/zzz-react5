import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'

export default class Page extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div {...this.requestProps()} className="container-fluid">
    <h1>{this.props.title}</h1>
      {this.props.children}
    </div>);
  }
}

Page.propTypes = {
};
