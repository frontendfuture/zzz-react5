import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'
import {observable, asMap, asFlat} from 'mobx'
import {observer} from 'mobx-react'

@observer
export default class List extends BaseComponent {
  constructor(props) {
    super(props);
    this.database = this.props.app.helpers.get('R5Firebase').getDatabase();
  }

  @observable dataList = asMap({})

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div>
      <ul className="list-group">
        {this.iterateList()}
      </ul>
    </div>);
  }

  componentDidMount() {
    this.loadData()
  }

  iterateList(){
    /** convert to multidimensional array **/
    let iteratedList = []
    if(this.dataList.get(this.POM.identifier)){
         iteratedList = this.dataList.get(this.POM.identifier).sort((a,b)=>{return b.timestamp - a.timestamp}).map((item,index)=>{
          let objKeys = Object.keys(item)
          let objKeysToShow = Array.prototype.concat([], objKeys)
              objKeysToShow = objKeysToShow.filter(i=>i!='key' && i!='timestamp')

          return (<li key={item.key} className="list-group-item">
            {objKeysToShow.map((field, idx)=>{
              return <span key={idx}>{item[field]}</span>
            })}
          </li>)
        })
    } else {
       iteratedList = []
    }
    return iteratedList
  }

  /**
   * get data from server and return it as this format:
   * [{key:value}]
   */
  loadData(){
    console.log('loading ref of ', this.POM.config.ref);
    this.database.ref(this.POM.config.ref).orderByKey().limitToLast(5).on('value', (snapshot)=>{
      var SnapArray = []
      console.log(snapshot.val(), 'SNAPSHOT');
      for(var item in snapshot.val()){
        SnapArray.push(Object.assign({}, snapshot.val()[item], {key: item}))
      }
      console.log(this, 'THIS');
      console.log(this.dataList, 'dataList');
      console.log(this.dataList, 'DL');
      this.dataList.set(this.POM.identifier, SnapArray);
    })
  }

}

List.propTypes = {
};
