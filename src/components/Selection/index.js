import 'react-select/dist/react-select.css'

import { extendObservable } from 'mobx';
import {observer} from 'mobx-react'
import React from 'react';
import Select from 'react-select'

import BaseComponent from '../BaseComponent/index'

@observer
export default class Selection extends BaseComponent {
  constructor(props) {
    super(props);
    this.options = this.POM.config.defaultOptions

    let state = {}
    extendObservable(state, {
      selected: null
    })
    this.state = state;
  }

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div style={{marginBottom: '20px'}}>
      <Select options={this.options}
        onChange={(value)=>{this.state.selected = value}}
        placeholder={this.POM.props.placeholder ? this.POM.props.placeholder : 'Pilih salah satu'}
        value={this.state.selected}
        />
    </div>);
  }

  componentDidMount() {
    this.loadData()
  }


  /**
   * get data from server and return it as this format:
   * [{key:value}]
   */
  loadData(){
    let dataPegawai = [
      {name: 'Andhika Jati', age: 26},
      {name: 'Andriansyah', age: 27}
    ]

    let dataBuku = [
      {name: 'Buku Bagus', year: 2017},
      {name: 'Amazing Buku', year: 2018}
    ]

    this.dataList = dataPegawai
  }

}

Selection.propTypes = {
};
