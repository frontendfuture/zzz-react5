import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'

export default class Row extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div className="row">
      {this.props.children}
    </div>);
  }
}

Row.propTypes = {
};
