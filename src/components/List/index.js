import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'
import {observable, asFlat} from 'mobx'
import {observer} from 'mobx-react'

@observer
export default class List extends BaseComponent {
  constructor(props) {
    super(props);
  }

  @observable dataList = asFlat([])

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div>
      <ul className="list-group">
        {this.iterateList()}
      </ul>
    </div>);
  }

  componentDidMount() {
    console.log(this.POM.identifier + ' just mounted');
    this.loadData()
  }

  iterateList(){
    /** convert to multidimensional array **/
    return this.dataList.map((item,index)=>{
      let objKeys = Object.keys(item)
      return (<li key={index} className="list-group-item">
        {objKeys.map((field, idx)=>{
          return <span key={idx}>{item[field]} </span>
        })}
      </li>)
    })
  }

  /**
   * get data from server and return it as this format:
   * [{key:value}]
   */
  loadData(){
    let dataPegawai = [
      {name: 'Andhika Jati', age: 26},
      {name: 'Andriansyah', age: 27}
    ]

    let dataBuku = [
      {name: 'Buku Bagus', year: 2017},
      {name: 'Amazing Buku', year: 2018}
    ]

    this.dataList = dataPegawai
  }

}

List.propTypes = {
};
