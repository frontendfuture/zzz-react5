import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'

export default class Button extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div>
      <button {...this.requestProps()} className="btn btn-primary">{this.props.label}</button>
    </div>);
  }
}

Button.propTypes = {
};
