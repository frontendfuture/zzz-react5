import React, {PropTypes} from 'react';
import {observable} from 'mobx'
import BaseComponent from '../BaseComponent/index'

export default class TextArea extends BaseComponent {
  constructor(props) {
    super(props);
  }
  @observable value = ''

  render() {
    return (<div className="control-group" style={{marginBottom:'20px'}}>
      <textarea type="text"
        className="form-control"
        {...this.requestProps()}
        onChange={this.mergeHandler.bind(this, 'onChange')}
    ></textarea>
  </div>);
  }

  onChange(e){
    let value = e.target.value;
    this.POM.setProp('value', value)
  }
}

TextArea.propTypes = {
};
