import React, {PropTypes} from 'react';
import Form from '../Form/index'
let defaultProps = {
  method: 'POST'
}
export default class FormAjax extends Form {
  constructor(props) {
    super(props);
  }

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div>
      <form {...this.extendedRequestProps()} onSubmit={this.mergeHandler.bind(this, 'onSubmit')}>
        {this.props.children}
      </form>
    </div>);
  }

  onSubmit(e){
    e.preventDefault()
    let endpoint = this.extendedRequestProps().method == 'PUT'
      ? this.POM.config.endpoint + '/' + this.POM.config.model_id
      : this.POM.config.endpoint
    this.props.app.helpers.get('R5Ajax')[this.extendedRequestProps().method.toLowerCase()]
    (endpoint, this.getDataMap())
    .done(()=>{
      this.POM.publish("submit::success")
    })
  }
}

FormAjax.propTypes = {
};
