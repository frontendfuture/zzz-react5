import {observable} from 'mobx'
import React from 'react';

import Input from '../Input';

export default class InputNumber extends Input {
  constructor(props) {
    super(props);
  }
  @observable value = ''

  render() {
    return (<div className="control-group" style={{marginBottom:'20px'}}>
      <input type="number"
        className="form-control"
        {...this.requestProps()}
        onChange={this.mergeHandler.bind(this, 'onChange')}
    />
  </div>);
  }

  onChange(e){
    let value = e.target.value;
    this.POM.setProp('value', value)
  }
}

InputNumber.propTypes = {
};
