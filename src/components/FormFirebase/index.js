import React, {PropTypes} from 'react';
import Form from '../Form/index'
import * as firebase from 'firebase';

let defaultProps = {
  method: 'POST'
}
export default class FormFirebase extends Form {
  constructor(props) {
    super(props);
    this.database = this.props.app.helpers.get('R5Firebase').getDatabase()
  }

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    return props;
  }

  render() {
    return (<div>
      <form {...this.extendedRequestProps()} onSubmit={this.mergeHandler.bind(this, 'onSubmit')}>
        {this.props.children}
      </form>
    </div>);
  }


  onSubmit(e){
    e.preventDefault()
    let key = this.database.ref(this.POM.config.ref).push().key

    var update = {}
        update[key] = this.getDataMap()
        console.log('CREATE NEW DATA', this.getDataMap());

    this.database.ref(this.POM.config.ref).update(update).then(()=>{
    }, ()=>{
      this.clearDataMap()
    })
    this.clearDataMap()
  }
}

FormFirebase.propTypes = {
};
