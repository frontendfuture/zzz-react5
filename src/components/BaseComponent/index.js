import React, {PropTypes} from 'react';
import {observable, computed, asMap} from 'mobx'
import {observer} from 'mobx-react'

@observer
export default class BaseComponent extends React.Component {
  constructor(props) {
    super(props);
    this.POM = props.pom;
  }

 /**
  * requestProp
  * @param none
  * @return Object props
  * filter & combine properties from POM, remove any unknow properties like children,
  * doc, app. To avoid React unknown props warning
  */
  requestProps(){
    let props = Object.assign({}, this.props)
    delete props.children
    delete props.doc
    delete props.app
    delete props.config
    delete props.pom
    return props
  }

  /**
   * margeHandler
   * @param String evname, Object event
   * @return void
   * Merging event handler from this component with that handler defined
   * by user in Page Definition. Then call both
   */
  mergeHandler(evname,e){
    this[evname](e) // internal handler
    // definition handler
    var eventDef = this.POM.eventsMap['onChange'];
    if(typeof eventDef === 'function'){eventDef(e)}
  }



  render() {
    return (<div {...this.requestProps()}>
      {this.props.title}
      {this.props.children}
    </div>);
  }
}

BaseComponent.propTypes = {
};
