export default function(){
  return (<div id="wrapper">
            {/* Custom CSS */}
            <link href="https://blackrockdigital.github.io/startbootstrap-sb-admin/css/sb-admin.css" rel="stylesheet" />
            {/* Custom Fonts */}
            <link href="https://blackrockdigital.github.io/startbootstrap-sb-admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <div id="wrappers">
              {/* Navigation */}
              <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                {/* Brand and toggle get grouped for better mobile display */}
                <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                  </button>
                  <a className="navbar-brand" href="index.html">{this.props.title}</a>
                </div>
                {/* Top Menu Items */}
                <ul className="nav navbar-right top-nav">
                  <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-envelope" /> <b className="caret" /></a>
                    <ul className="dropdown-menu message-dropdown">
                      <li className="message-preview">
                        <a href="#">
                          <div className="media">
                            <span className="pull-left">
                              <img className="media-object" src="http://placehold.it/50x50" alt />
                            </span>
                            <div className="media-body">
                              <h5 className="media-heading">
                              </h5>
                              <p className="small text-muted"><i className="fa fa-clock-o" /> Yesterday at 4:32 PM</p>
                              <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li className="message-preview">
                        <a href="#">
                          <div className="media">
                            <span className="pull-left">
                              <img className="media-object" src="http://placehold.it/50x50" alt />
                            </span>
                            <div className="media-body">
                              <h5 className="media-heading">
                                <strong>John Smith</strong>
                              </h5>
                              <p className="small text-muted"><i className="fa fa-clock-o" /> Yesterday at 4:32 PM</p>
                              <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li className="message-preview">
                        <a href="#">
                          <div className="media">
                            <span className="pull-left">
                              <img className="media-object" src="http://placehold.it/50x50" alt />
                            </span>
                            <div className="media-body">
                              <h5 className="media-heading">
                                <strong>John Smith</strong>
                              </h5>
                              <p className="small text-muted"><i className="fa fa-clock-o" /> Yesterday at 4:32 PM</p>
                              <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li className="message-footer">
                        <a href="#">Read All New Messages</a>
                      </li>
                    </ul>
                  </li>
                  <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-bell" /> <b className="caret" /></a>
                    <ul className="dropdown-menu alert-dropdown">
                      <li>
                        <a href="#">Alert Name <span className="label label-default">Alert Badge</span></a>
                      </li>
                      <li>
                        <a href="#">Alert Name <span className="label label-primary">Alert Badge</span></a>
                      </li>
                      <li>
                        <a href="#">Alert Name <span className="label label-success">Alert Badge</span></a>
                      </li>
                      <li>
                        <a href="#">Alert Name <span className="label label-info">Alert Badge</span></a>
                      </li>
                      <li>
                        <a href="#">Alert Name <span className="label label-warning">Alert Badge</span></a>
                      </li>
                      <li>
                        <a href="#">Alert Name <span className="label label-danger">Alert Badge</span></a>
                      </li>
                      <li className="divider" />
                      <li>
                        <a href="#">View All</a>
                      </li>
                    </ul>
                  </li>
                  <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user" /> John Smith <b className="caret" /></a>
                    <ul className="dropdown-menu">
                      <li>
                        <a href="#"><i className="fa fa-fw fa-user" /> Profile</a>
                      </li>
                      <li>
                        <a href="#"><i className="fa fa-fw fa-envelope" /> Inbox</a>
                      </li>
                      <li>
                        <a href="#"><i className="fa fa-fw fa-gear" /> Settings</a>
                      </li>
                      <li className="divider" />
                      <li>
                        <a href="#"><i className="fa fa-fw fa-power-off" /> Log Out</a>
                      </li>
                    </ul>
                  </li>
                </ul>
                {/* Sidebar Menu Items - These collapse to the responsive navigation menu on small screens */}
                <div className="collapse navbar-collapse navbar-ex1-collapse">
                  <ul className="nav navbar-nav side-nav">
                    {this.POM.config.menus.map((menu,index)=>{
                      return (
                        <li key={index} onClick={this.onMenuClick.bind(this, menu)}>
                          <a href={menu.link}><i className={menu.iconClass} />{menu.label}</a>
                        </li>
                      )
                    })}
                  </ul>
                </div>
                {/* /.navbar-collapse */}
              </nav>
              <div id="page-wrapper">
                <div className="container-fluid">
                  {/* Page Heading */}
                  <div className="row">
                    <div className="col-lg-12">
                      {this.whichChildren()}
                    </div>
                  </div>
                  {/* /.row */}
                </div>
                {/* /.container-fluid */}
              </div>
              {/* /#page-wrapper */}
            </div>
            {/* /#wrapper */}
            {/* jQuery */}
            {/* Bootstrap Core JavaScript */}
      </div>)
}
