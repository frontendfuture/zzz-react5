import { extendObservable } from 'mobx';
import React from 'react';
import BaseComponent from '../BaseComponent/index'
import template from './themes/gantella/template.js'


export default class PageSidebar extends BaseComponent {
  constructor(props) {
    super(props);
    let state = {}
    extendObservable(state, {
      activeChildren: 0
    })
    this.state = state;
  }


  render() {
      return (<span>{template.apply(this)}</span>)
  }

  whichChildren(){
    return (<div>
      {this.props.children[this.state.activeChildren]}
    </div>)
  }

  onMenuClick(menu){
    if(menu.open.indexOf('http')==0){
      window.location.href = menu.open;
    } else {
      console.log(this.props.children, this.props.children.getIndexBy('key', 'magictable2'));
      this.state.activeChildren = this.props.children.getIndexBy('key', menu.open)
    }
  }
}

PageSidebar.propTypes = {
};
