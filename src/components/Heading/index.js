import React, {PropTypes} from 'react';
import BaseComponent from '../BaseComponent/index'


let defaultProps = {
  variant: 'h1'
}
export default class Heading extends BaseComponent {
  constructor(props) {
    super(props);
  }


  render() {
    return (<div {...this.requestProps()}>
    {this.parseHeading()}
    </div>);
  }

  parseHeading(){
    switch(this.POM.config.variant){
      case 'h1':
        return (<h1>{this.props.title}</h1>)
      break;
      default:
      // do nothing
      return (<h6>{this.props.title}</h6>)
    }
  }
}

Heading.propTypes = {
};
