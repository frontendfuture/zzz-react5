import React, {PropTypes} from 'react';
import ListFirebase from '../ListFirebase/index'
import TodoItem from './TodoItem/index'
import POM from '../../R5/POM'
import app from '../../R5/index'
import {observer} from 'mobx-react'
import {observable, asReference} from 'mobx'


let FBSignature = {
  "identifier": "form2",
  "type": "FormFirebase",
  "defaultValue": {
    "done": false
  },
  "children": [
    {
      "identifier": "title",
      "type":"Input",
      "props": {
        "placeholder": "Insert new todo here",
        "style": {
          "outline":"none",
          "borderRadius": "0px",
          "background": "lightyellow",
          "height":"50px"
        }
      },
      "on": {
        "change": {
        }
      }
    }
  ]
}

let defaultProps = {
  done: false
}

@observer
export default class TodoList extends ListFirebase {
  constructor(props) {
    super(props);
    this.createFormFirebase(props)
  }

  extendedRequestProps(){
    let props = Object.assign({}, defaultProps, this.requestProps())
    console.log('PROPS FROM INSIDE', props);
    return props;
  }

  createFormFirebase(props){
    FBSignature.ref = props.doc.definition.ref;
    this.FirebaseForm = new POM(FBSignature)
    FBSignature.children[0].props.placeholder = this.props.doc.definition.inputPlaceholder
    this.FirebaseForm.setChildren(FBSignature.children)
    app.registerPOM(this.FirebaseForm)
  }

  render(){
    return (<div {...this.requestProps()}>
      <h1>{this.props.title && this.props.title.length ? this.props.title : "TodoList"}</h1>
      <div className="row">
        {this.FirebaseForm.renderComponent}
      </div>
      <div className="row">
        {this.iterateList()}
      </div>
    </div>)
  }

  iterateList(){
    /** convert to multidimensional array **/
    let iteratedList = []
    if(this.dataList.get(this.props.doc.identifier)){
         iteratedList = this.dataList.get(this.props.doc.identifier).sort((a,b)=>{return b.timestamp - a.timestamp}).map((item,index)=>{
          let objKeys = Object.keys(item)
          let objKeysToShow = Array.prototype.concat([], objKeys)
              objKeysToShow = objKeysToShow.filter(i=>i!='key' && i!='timestamp')

          return (<TodoItem
              {...this.props}
              key={index}
              firebaseKey={item.key}
              todo={item}
              onUpdateTodo={this.updateTodo.bind(this)}
              onDeleteTodo={this.deleteTodo.bind(this)}
              >
            {objKeysToShow.map((field, idx)=>{
              return <span key={idx}>{item[field]}</span>
            })}
          </TodoItem>)

        })
    } else {
       iteratedList = []
    }
    return iteratedList
  }

  updateTodo(key, data){
    this.database.ref(this.props.doc.definition.ref + '/' + key).update(data)
  }

  deleteTodo(key){
    this.database.ref(this.props.doc.definition.ref + '/' + key).remove()
  }

  /**
   * get data from server and return it as this format:
   * [{key:value}]
   */
  loadData(){
    console.log('loading ref of ', this.props.doc.definition.ref);
    this.database.ref(this.props.doc.definition.ref).orderByKey().limitToLast(5).on('value', (snapshot)=>{
      var SnapArray = []
      for(var item in snapshot.val()){
        SnapArray.push(Object.assign({}, snapshot.val()[item], {key: item}))
      }
      this.dataList.set(this.props.doc.identifier, SnapArray);
    })
  }
}

TodoList.propTypes = {
};
