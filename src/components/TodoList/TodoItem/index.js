import React, {PropTypes} from 'react';
import BaseComponent from '../../BaseComponent/index'
import {observer} from 'mobx-react'
import {observable, asMap} from 'mobx'
import './style.css'
import Swal from 'sweetalert'
import 'sweetalert/dist/sweetalert.css'

@observer
export default class TodoItem extends BaseComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.done.set(this.props.firebaseKey, this.props.todo.done)
  }

  @observable done = asMap({})

  render() {
    return (<div style={{position: 'relative'}}>
      <div style={{cursor: 'pointer', borderRadius: '0px', boxShadow: '1px 1px 1px #eee', background: '#eee'}}
    onClick={this.mergeHandler.bind(this, 'onClick')}
    onDoubleClick={this.onRequestEdit.bind(this)}
    className={this.props.todo.done ? 'list-group-item todo-done': 'list-group-item todo-undone'}
    >
      <input type="checkbox"
        checked={this.done.get(this.props.firebaseKey) ? true : false}
        onChange={this.onClick.bind(this)}
        style={{
          borderRadius: '0px',
          marginRight: '10px',
        }}
        />
      {this.props.children}
    </div>
    <span className="label label-danger" style={{float: 'right', position: 'absolute', top: '10px', right: '5px', cursor: 'pointer'}}
      onClick={this.onDelete.bind(this)}
      >
      delete
    </span>
      </div>);
  }

  onRequestEdit(){
    Swal({
      title: 'Enter new task name...',
      type: 'input',
      showCancelButton: true,
      closeOnConfirm: true,
    }, (value)=>{
      if(value === false)return false;
      if(value === ""){
        Swal.showInputError("Can't be blank")
      }
      if(value && value.length){
        this.props.onUpdateTodo(this.props.firebaseKey, {title: value})
      }
    })
  }

  onDelete(e){
    e.preventDefault();
    e.stopPropagation();
    this.props.onDeleteTodo(this.props.firebaseKey)
  }

  onClick(e){
    e.stopPropagation()
    this.done.set(this.props.firebaseKey, !this.props.todo.done)
    this.props.onUpdateTodo(this.props.firebaseKey, {done:!this.props.todo.done})
  }
}

TodoItem.propTypes = {
};
