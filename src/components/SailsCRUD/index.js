import { action, extendObservable, observable } from 'mobx';
import { observer } from 'mobx-react'
import React from 'react';
import AddForm from './AddForm';
import BaseComponent from '../BaseComponent/index'
import EditForm from './EditForm/index'
import Header from './Header';
import TableView from './TableView/index'



@observer
export default class SailsCRUD extends BaseComponent {

  @observable pagination = {
    skip: 0,
    limit: 10
  }
  constructor(props) {
    super(props)
    let state = {};
    extendObservable(state, {
      dataList: [],
      pagination: {
        skip:0,
        limit:10
      },
      activeScreen: 'table',
      itemToEdit: null
    })
    this.state = state;
  }
  componentDidMount() {
    this.loadData()
  }

  render(){
    let isListing = this.state.activeScreen == 'table' ? {} : {display: 'none'}
    let isEditing = this.state.activeScreen == 'editing' ? {} : {display: 'none'}
    let isAdding = this.state.activeScreen == 'adding' ? {} : {display: 'none'}
    return (<div>
      <Header
        onChangeScreen={this.onChangeScreen.bind(this)}
        />

      <TableView style={isListing}
        {...this.props}
        onChangeScreen={this.onChangeScreen.bind(this)}
        onReload={()=>{this.loadData()}}
        onUpdatePage={this.updateData.bind(this)}
        goTo={this.goTo.bind(this)}
        data={this.state.dataList.slice()}
        pagination={this.pagination}
        />

      <EditForm
        {...this.props}
        style={isEditing}
        item={this.state.itemToEdit}
        onChangeScreen={this.onChangeScreen.bind(this)}
        onReload={()=>{this.loadData()}}
        />

      <AddForm
        {...this.props}
        style={isAdding}
        onChangeScreen={this.onChangeScreen.bind(this)}
        onReload={()=>{this.loadData()}}
        />
    </div>)
  }

  goTo(nextOrPref){
    if(nextOrPref == 'next'){
      if(this.state.dataList.slice().length){
        this.pagination.skip += this.pagination.limit;
      }
    } else {
      if(this.pagination.skip - this.pagination.limit >= 0){
        this.pagination.skip -= this.pagination.limit;
      } else {
        this.pagination.skip = 0;
      }
    }

    this.loadData()
  }

  onChangeScreen(screen, item=null){
    this.state.activeScreen = screen;
    if(item){this.state.itemToEdit = item}
  }

  updateData(payload){
    for(var field in payload){
      this.pagination[field] = payload[field]
    }
    this.loadData();
  }

  @action loadData(){
    $.ajax({
      method: 'GET',
      url: this.POM.config.endpoint,
      data: {
        sort: 'ID DESC',
        skip: this.pagination.skip,
        limit: this.pagination.limit,
      },
      success: (results)=>{
        this.state.dataList = results
      },
      error: (reason)=>{
      }
    })
  }
}

SailsCRUD.propTypes = {
};
