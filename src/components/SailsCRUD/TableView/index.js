import { observer } from 'mobx-react'
import React from 'react';

import BaseComponent from '../../BaseComponent/index'
import Swal from 'sweetalert'
import 'sweetalert/dist/sweetalert.css'

@observer
export default class TableView extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div style={this.props.style}>
      THE NEW SailsCRUD

      <table className="table table-striped table-bordered">
      <thead>
        <tr>
          {this.props.data[0] ? Object.keys(this.props.data[0]).filter(x => x != 'id' && x != 'createdAt' && x != 'updatedAt').map((field, index)=>{
            return <th key={index}>{field.split(' ').map(x=>x.charAt(0).toUpperCase() + x.substr(1)).join(' ')}</th>
          }): (<th></th>)}
          <th style={{width: '150px', textAlign: 'right'}}>Action</th>
        </tr>
      </thead>
      <tbody>
        {this.iterateList()}
      </tbody>
      <tfoot>
        <tr>
          <td style={{textAlign: 'center'}} colSpan={10}>
            <nav aria-label="Page navigation">
              <ul className="pagination">
                <li>
                  <a aria-label="Previous" onClick={()=>{this.props.goTo('previous')}}>
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li>
                  <a aria-label="Next" onClick={()=>{this.props.goTo('next')}}>
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </td>
        </tr>
      </tfoot>
      </table>

    </div>);
  }

  onTextChange(type, e){
    let payload = {}
        payload[type] = e.target.value;
    if(type == 'page'){
      this.props.onUpdatePage({skip: (e.target.value * this.props.pagination.limit)})
    } else {
      this.props.onUpdatePage(payload)
    }
  }


  iterateList(){
    /** convert to multidimensional array **/
    let data = this.props.data.map((item, index)=>{delete item.createdAt; delete item.updatedAt; return item})
    return data.map((item,index)=>{
      let objKeys = Object.keys(item)
      return (<tr key={index}>
        {objKeys.map((field, idx)=>{
          return <td style={field == 'id' ? {display: 'none'} : {width: '100px'}} key={idx}>{item[field]} </td>
        })}
        <td style={{textAlign: 'right'}}>
          <span style={{marginRight: '10px'}} className="btn btn-danger btn-xs" onClick={this.requestDelete.bind(this, item.id)}>hapus</span>
          <span className="btn btn-info btn-xs" onClick={this.requestEdit.bind(this, item)}>edit</span>
        </td>
      </tr>)
    })
  }


  requestEdit(item, e){
    e.stopPropagation();
    this.props.onChangeScreen('editing', item)
  }

  requestDelete(id,e){
    e.stopPropagation()
    Swal({
      type: 'warning',
      title: 'Hapus Item?',
      text: 'Hati-hati, hapus berarti data tidak bisa dikembalikan lagi!',
      showCancelButton: true,
    }, (value)=>{
      if(value){
        $.ajax({
          method: 'DELETE',
          url: this.POM.config.endpoint + '/' + id,
          success: ()=>{
            this.props.onReload()
          }
        })
      }
    })
  }
}

TableView.propTypes = {
};
