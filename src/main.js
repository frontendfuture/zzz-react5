import './themes/default.theme.css'

import {observer} from 'mobx-react'
import { render } from 'react-dom'
import React, { Component } from 'react'

import {pages} from './pages/index'
import POM from './R5/POM'
import R5Ajax from './R5/helpers/R5Ajax'
import R5Firebase from './R5/helpers/R5Firebase'
import app from './R5/index'

    if(typeof definition === 'undefined'){
      let definition = getParameterByName('p')
      definition = pages[definition]
      console.log(definition, 'DEFINITION');
      var page = new POM(definition)
      page.setChildren(definition.children)
      app.registerPOM(page)
      app.registerHelper(R5Ajax)
    } else {
      console.log(definition, 'DEFINITION');
      var page = new POM(definition)
      page.setChildren(definition.children)
      app.registerPOM(page)
      app.registerHelper(R5Ajax)
    }



    app.registerHelper(new R5Firebase({
      apiKey: "AIzaSyDASe0pMP0TCX-vQ0wNJ2CUAMyr2epaI7k",
      authDomain: "ideabox-814b6.firebaseapp.com",
      databaseURL: "https://ideabox-814b6.firebaseio.com",
      storageBucket: "ideabox-814b6.appspot.com",
      messagingSenderId: "448199022548"
    }))


@observer
export default class App extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount(){

  }

  render(){
    return (<span>
      {page.renderComponent}
    </span>)
  }
}

// add new element as render point
$('body').append('<div id="app"></div>')


// plug react app to the render point
var render_point = document.getElementById('app');
render(<App/>, render_point);


function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
