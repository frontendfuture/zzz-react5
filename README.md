#R5 Framework

## Folder Structure
* components
    * BaseComponent
    * All Other Component (inherits BaseComponent)
* pages (define your page)
* R5 (Core Framework)
    * helpers
        * R5Ajax (ajax helper using jQuery)
    * POM.js (Page Object Model)
    * index.js (The R5 starting point)


## Page Definition
Define your page a.k.a App only using JSON definition.
every element, including page is just component with this signature


        {
            "identifier": "idofthiscomponent", // must unique and follow variable convention
            "type": "ComponentType" // the react component type,
            "props": {
              "prop1": "value" // property value pair,
              // ... etc ...//
            },
            "on": {
              "click": {
                "doc_identifier": [{"doSomething": ["param1", "param2"]}]
              }
            }
        }
